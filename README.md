 Nemesis Advanced

    Case - Nemesis Case - White [Exterior Multi-color LED]
    Noise Reduction - No Noise Reduction
    Internal Lighting - None
    Processor - Intel Core i7-6700 3.4GHz (Quad Core)
    CPU Cooling - Asetek 550LC High Performance Liquid Cooling
    PC Liquid Coolant - *Requires Iron Tundra Liquid Cooling Systems
    Motherboard - Z170 Chipset Motherboard
    Memory - 16GB (2x8GB) DDR4 2133Mhz
    Primary Hard Drive - 256GB Ironside Titanium Series SSD
    Secondary Hard Drive - 2TB 7200 RPM
    Optical Drive - None
    Graphics Card - Geforce GTX 1070 8GB (Min. 500 Watt Power Supply)
    Power Supply - Standard 600 Watt
    Operating System - Windows 10 64 Bit
    Networking - Standard Onboard Ethernet (No Wi-fi)
    Capture Card - None
    Office Software - None
    Monitor - None
    Mouse - None
    Mouse Pad - None
    Keyboard - None
    Controller - None
    Headsets - None
    Speakers - None
    Beanie - None
    T-Shirt - None
    Hoodie - None
    Packaging - Standard Packaging System - Packing peanuts protect and secure internal components from shipping abuse
    Discrete Packaging - Ironside Branding on Packaging
    Wiring - [FREE] Professional Wiring - Cables will be organized to achieve maximum airflow
    Technical Support - Life-time U.S. Based Technical Support
    Assembly Time - Standard - 10 Business Days or Less for Assembly Before Shipping
    Warranty - Standard 5 Years Labor and 3 Years Parts
    Repair Shipping Coverage Plan - None
